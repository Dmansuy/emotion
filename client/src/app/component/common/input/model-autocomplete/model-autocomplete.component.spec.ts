import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelAutocompleteComponent } from './model-autocomplete.component';

describe('ModelAutocompleteComponent', () => {
  let component: ModelAutocompleteComponent;
  let fixture: ComponentFixture<ModelAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should createCustomer', () => {
    expect(component).toBeTruthy();
  });
});
