import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AvailableVehiclesComponent} from './rental.component';

describe('RentalComponent', () => {
  let component: AvailableVehiclesComponent;
  let fixture: ComponentFixture<AvailableVehiclesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AvailableVehiclesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailableVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should createCustomer', () => {
    expect(component).toBeTruthy();
  });
});
