import {Component, Input, OnInit} from '@angular/core';
import {VehicleService} from "../../../../service/vehicle.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Model} from "../../../../../model/input/Model";
import {BusinessUtils} from "../../../../../utils/BusinessUtils";

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {

  model: Model;

  @Input()
  name: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly vehicleService: VehicleService
  ) {
    this.vehicleService.getOneModel(route.snapshot.params['id'])
                       .subscribe(model => this.model = model);
  }

  ngOnInit() {
  }

  isCar(model: Model): boolean {
    return BusinessUtils.isCar(model);
  }

  updateModel(): void {
    this.vehicleService.updateModel(this.route.snapshot.params['id'], this.name)
                       .subscribe();
  }

  deleteModel(): void {
    this.vehicleService.deleteOneModel(this.route.snapshot.params['id'])
                       .subscribe(
                         () =>
                           this.router.navigateByUrl('/modele/liste')
                       );
  }
}
