import {Component, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {TokenHolder} from "../../../../helper/token-holder.service";
import {Customer} from "../../../../../model/input/Customer";
import {UserService} from "../../../../service/user.service";
import {create, DetailsUpdate} from "../../../../../model/output/DetailsUpdate";
import {Type} from "../../../../../model/input/User";
import {Error} from "tslint/lib/error";
import {TokenCreation} from "../../../../../model/output/TokenCreation";
import {StringUtils} from "../../../../../utils/StringUtils";
import "rxjs-compat/add/observable/of";
import {RouteUtils} from "../../../../../utils/RouteUtils";

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  customer: Customer;

  wrongCurrentPassword: boolean;

  @Input()
  detailsUpdate: DetailsUpdate;

  @Input()
  tokenCreation: TokenCreation;

  @Input()
  newPassword: string;

  @Input()
  newPasswordConfirm: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly tokenHolder: TokenHolder,
    private readonly userService: UserService
  ) {

    let initModel: (customer: Customer) => void;

    initModel = customer => {
      this.detailsUpdate = create(
        customer.address,
        customer.zipCode,
        customer.city,
      );
    };

    this.tokenCreation = {
      emailAddress: null,
      password: null,
    };

    if(!this.userAdmin) {
      this.customer = tokenHolder.token.user as Customer;
      initModel(this.customer);
    } else
      this.userService.getOneCustomer(this.route.snapshot.params['id'])
                      .subscribe(
                        customer => {
                          this.customer = customer;
                          initModel(customer);
                          this.tokenCreation.emailAddress = customer.emailAddress;
                        }
                      );
  }

  get userAdmin(): boolean {
    return this.tokenHolder.token.user.__type__ == Type.ADMIN;
  }

  isDetailsCompleted(customer: Customer): boolean {
    return this.detailsUpdate.address != customer.address ||
           this.detailsUpdate.zipCode != customer.zipCode ||
           this.detailsUpdate.city    != customer.city;
  }

  get passwordsMatch(): boolean {
    return this.newPasswordConfirm ?
      this.newPassword == this.newPasswordConfirm :
      true;
  }

  get passwordCompleted(): boolean {
    return !StringUtils.areBlank([
        this.newPassword,
        this.newPasswordConfirm,
        this.tokenCreation.password
      ]) &&
      this.passwordsMatch &&
      this.newPassword != this.tokenCreation.password;
  }

  resetPasswordUpdate(): void {
    this.newPasswordConfirm = this.newPassword
                            = this.tokenCreation.password
                            = null;
  }

  updateCustomer(ordinal: number): void {
    let id: number;

    if(this.userAdmin)
      id = this.route.snapshot.params['id'];
    else
      id = this.tokenHolder.token.user.id;

    switch(ordinal) {
      case 0:
        this.userService.updateCustomer(id, this.detailsUpdate)
                        .subscribe(() => {
                          const customer: Customer = this.tokenHolder.token.user as Customer;
                          customer.address = this.detailsUpdate.address;
                          customer.zipCode = this.detailsUpdate.zipCode;
                          customer.city    = this.detailsUpdate.city;
                          this.tokenHolder.token.user = customer;
                        });
        break;
      case 1:
        this.userService.createToken(this.tokenCreation)
                        .subscribe(
                          token => {
                              this.userService.update(id, this.newPassword).subscribe(
                                () => {
                                  this.resetPasswordUpdate();

                                  this.tokenHolder.token = null;

                                  RouteUtils.refresh(this.router);
                                }
                              );
                          },
                          () =>
                            this.wrongCurrentPassword = true
                        );

        break;
      default:
        throw new Error;
    }
  }

  deleteCustomer(): void {
    this.userService.deleteOneCustomer(this.customer.id)
                    .subscribe(
                      () => this.router.navigateByUrl('/client/liste')
                    );
  }

  ngOnInit() {
  }

}
