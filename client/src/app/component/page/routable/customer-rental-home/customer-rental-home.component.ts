import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {RentalService} from "../../../../service/rental.service";
import {Rental} from "../../../../../model/input/Rental";
import {ActivatedRoute} from "@angular/router";
import {TokenHolder} from "../../../../helper/token-holder.service";

@Component({
  selector: 'app-customer-rental-home',
  templateUrl: './customer-rental-home.component.html',
  styleUrls: ['./customer-rental-home.component.css']
})
export class CustomerRentalHomeComponent implements AfterViewInit {

  rentals: Rental[];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly tokenHolder: TokenHolder,
    private readonly rentalService: RentalService
  ) {}

  ngAfterViewInit() {

    this.rentalService
        .getByCustomerId(this.tokenHolder.token.user.id)
        .subscribe(
          rentals => this.rentals = rentals
        );
  }

}
