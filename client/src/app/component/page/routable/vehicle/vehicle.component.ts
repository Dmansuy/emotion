import {Component, Input, OnInit} from '@angular/core';
import {VehicleService} from "../../../../service/vehicle.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Vehicle} from "../../../../../model/input/Vehicle";
import {KilometerageUpdate, create as create0} from "../../../../../model/output/KilometerageUpdate";
import {RentalPriceAndLoyaltyPointsUpdate, create as create1} from "../../../../../model/output/RentalPriceAndLoyaltyPointsUpdate";
import {Error} from "tslint/lib/error";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

  vehicle: Vehicle;

  @Input()
  kilometerageUpdate: KilometerageUpdate;

  @Input()
  rentalPriceAndLoyaltyPointsUpdate: RentalPriceAndLoyaltyPointsUpdate;


  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly vehicleService: VehicleService
  ) {
    this.vehicleService.getOne(this.route.snapshot.params['id'])
                       .subscribe(vehicle => {
                            this.vehicle = vehicle;
                            this.kilometerageUpdate = create0(vehicle.numberOfKilometers);
                            this.rentalPriceAndLoyaltyPointsUpdate = create1(
                              vehicle.rentalPrice,
                              vehicle.loyaltyPoints
                            );
                         }
                       );
  }

  ngOnInit() {
  }

  updateVehicle(ordinal: number): void {
    let id: number;
    let observable: Observable<void>;

    id = this.route.snapshot.params['id'];

    switch(ordinal) {
      case 0:
        observable = this.vehicleService.update(
          id,
          this.kilometerageUpdate
        );
        break;
      case 1:
        observable = this.vehicleService.update(
          id,
          this.rentalPriceAndLoyaltyPointsUpdate
        );
        break;
      default:
        throw new Error;
    }

    observable.subscribe();
  }

  deleteVehicle(): void {
    this.vehicleService.deleteOneMake(this.route.snapshot.params['id'])
                       .subscribe(
                         () =>
                           this.router.navigateByUrl('/vehicule/liste')
                       );
  }
}
