import { Component, OnInit } from '@angular/core';
import {Rental} from "../../../../../model/input/Rental";
import {RentalService} from "../../../../service/rental.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TokenHolder} from "../../../../helper/token-holder.service";
import {Type, User} from "../../../../../model/input/User";
import {Observable} from "rxjs/Observable";
import {Error} from "tslint/lib/error";
import {Customer} from "../../../../../model/input/Customer";
import {Location} from "@angular/common";
import {create} from "../../../../../model/output/OverdueDurationUpdate";

const DAY_TO_MILLI_VAL: number = 1000 * 3600 * 24;

@Component({
  selector: 'app-rental',
  templateUrl: './rental.component.html',
  styleUrls: ['./rental.component.css']
})
export class RentalComponent implements OnInit {

  rental: Rental;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly location: Location,
    private readonly router: Router,
    private readonly tokenHolder: TokenHolder,
    private readonly rentalService: RentalService
  ) {
    const rentalId: number = parseInt(this.route.snapshot.params['rentalId']);
    const user: User = this.tokenHolder.token.user;

    let rentalObservable : Observable<Rental>;

    switch(user.__type__) {
      case Type.ADMIN:
        rentalObservable = this.rentalService.getOne(rentalId);
        break;
      case Type.CUSTOMER:
        if((user as Customer).rentalIds.indexOf(rentalId) != -1)
          rentalObservable = this.rentalService.getOneByCustomerId(rentalId, user.id);
        else {

          const currentUrl: string = this.route.snapshot.url.join('/');

          this.router.navigateByUrl(
            '/403',
            {
              skipLocationChange: true,
              replaceUrl: false,
            }
          ).then(() => this.location.replaceState(currentUrl));
          return;
        }
        break;
      default:
        throw new Error;
    }

    rentalObservable.subscribe(rental => this.rental = rental);
  }

  ngOnInit() {
  }

  get now(): number {
    return Date.now();
  }

  get dropoffDate(): Date {
    return new Date(
      this.rental.pickupDate.getTime() +
      (this.rental.duration || 0) * DAY_TO_MILLI_VAL
    );
  }

  get effectiveDropoffDate(): Date {
    return new Date(
      this.dropoffDate.getTime() +
      (this.rental.overdueDuration || 0)  * DAY_TO_MILLI_VAL
    );
  }

  get remainingDays(): number { console.log(this.rental.overdueDuration == null);
    return this.rental.overdueDuration == null ?
        Math.floor(
        (this.dropoffDate.getTime() - Date.now()) /
        DAY_TO_MILLI_VAL
    ) : -Math.floor(
        (this.dropoffDate.getTime() + this.rental.overdueDuration / DAY_TO_MILLI_VAL) /
        DAY_TO_MILLI_VAL
      );
  }

  get userAdmin(): boolean {
    return this.tokenHolder.token.user.__type__ == Type.ADMIN;
  }

  get futureRental(): boolean {
    return this.rental.pickupDate.getTime() > Date.now();
  }

  updateRental(): void {
    this.rentalService.update(
                        this.rental.id,
                        create(-this.remainingDays)
                      )
                      .subscribe(
                        () =>
                          this.router.navigateByUrl(this.router.url)
                      );
  }

  deleteRental(): void {

    let observable: Observable<void>;

    observable = this.rentalService.deleteOne(this.rental.id);

    observable.subscribe(
      () => this.router.navigateByUrl('/location/liste')
    );
  }
}
