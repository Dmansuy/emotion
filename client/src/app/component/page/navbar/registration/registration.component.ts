import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserService} from "../../../../service/user.service";
import {CustomerCreation} from "../../../../../model/output/CustomerCreation";
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  @Input()
  customerCreation: CustomerCreation;

  @Input()
  passwordConfirm: string;

  @Output("customerCreated")
  eventEmitter: EventEmitter<void>;

  constructor(private readonly userService: UserService, private readonly router: Router) {
    this.resetCustomerCreation();

    this.eventEmitter = new EventEmitter;
  }

  ngOnInit() {
  }

  get passwordsMatch(): boolean {
    return this.passwordConfirm ?
        this.customerCreation.password == this.passwordConfirm :
        true
  }

  get completed(): boolean {
    for(let key of Object.keys(this.customerCreation))
      if(!this.customerCreation[key])
        return false;
    return this.passwordConfirm && this.passwordsMatch;
  }

  createCustomer(): void {
    this.userService
        .createCustomer(this.customerCreation)
        .subscribe(() => {
          this.eventEmitter.emit();
          this.resetCustomerCreation();
          this.router.navigateByUrl(this.router.url);
        });
  }

  private resetCustomerCreation(): void {
    this.customerCreation = {
      emailAddress: null,
      password: null,
      firstName: null,
      lastName: null,
      birthDate: null,
      address: null,
      zipCode: null,
      city: null,
    };
  }
}
