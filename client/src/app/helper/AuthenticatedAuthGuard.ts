import {Type} from "../../model/input/User";
import {Injectable} from "@angular/core";
import {AuthGuard} from "./AuthGuard";
import {Observable} from "rxjs/Observable";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";

@Injectable()
export class AuthenticatedAuthGuard extends AuthGuard {

  protected support(): Type {
    return null;
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if(this.tokenHolder.token != null)
      return true;

    const currentUrl: string = state.url;

      this.router.navigateByUrl(
        '/401',
        {
          skipLocationChange: true,
          replaceUrl: false,
        }
      ).then(() => this.location.replaceState(currentUrl));

    return false;
  }
}
