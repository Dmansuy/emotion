import {LocalStorage} from "ngx-store";
import {Injectable} from "@angular/core";
import {Token} from "../../model/input/Token";

@Injectable()
export class TokenHolder {

  @LocalStorage()
  private _persistentToken: Token;

  private _nonPersistentToken: Token;

  get token(): Token {
    return this._persistentToken || this._nonPersistentToken;
  }

  set token(token: Token) {
    this._persistentToken = this._nonPersistentToken
                          = null;

    if(token != null)
      if(token.persistent)
        this._persistentToken = token;
      else
        this._nonPersistentToken = token;
  }
}
