import {Injectable} from '@angular/core';
import {VehicleSearch} from "../../model/output/VehicleSearch";
import {Observable} from "rxjs/Observable";
import {Service} from "./service";
import {Vehicle} from "../../model/input/Vehicle";
import {Make} from "../../model/input/Make";
import {Model, VehicleType} from "../../model/input/Model";
import {HttpParams} from "@angular/common/http";
import {MakeCreation} from "../../model/output/MakeCreation";
import {VehicleCreation} from "../../model/output/VehicleCreation";
import {VehicleUpdate} from "../../model/output/VehicleUpdate";
import {ModelCreation} from "../../model/output/ModelCreation";

@Injectable()
export class VehicleService extends Service {

  getAll(): Observable<Vehicle[]> {
    return this.httpClient.get<Vehicle[]>('http://' + this.apiConfig.server + '/vehicles');
  }

  getOne(id: number): Observable<Vehicle> {
    return this.httpClient.get<Vehicle>('http://' + this.apiConfig.server + '/vehicles/' + id);
  }

  get(vehicleSearch: VehicleSearch): Observable<Vehicle[]> {
    return this.httpClient.get<Vehicle[]>(
      'http://' + this.apiConfig.server + '/vehicles',
      {
        params: this.toHttpParams(vehicleSearch)
      }
    );
  }

  getAllMakes(): Observable<Make[]> {
    return this.httpClient.get<Make[]>('http://' + this.apiConfig.server + '/makes');
  }

  getMakesByVehicleType(vehicleType: VehicleType): Observable<Make[]> {
    let httpParams: HttpParams;

    httpParams = new HttpParams;

    httpParams = httpParams.append('vehicleType', VehicleType[vehicleType]);

    return this.httpClient.get<Make[]>(
      'http://' + this.apiConfig.server + '/makes',
      {
        params: httpParams,
      }
    );
  }

  getOneMake(id: number): Observable<Make> {
    return this.httpClient.get<Make>('http://' + this.apiConfig.server + '/makes/' + id);
  }

  getAllModels(): Observable<Model[]> {
    return this.httpClient.get<Model[]>('http://' + this.apiConfig.server + '/models');
  }

  getModels(makeId: number): Observable<Model[]> {
    return this.httpClient.get<Model[]>('http://' + this.apiConfig.server + '/makes/' + makeId + '/models');
  }

  getModelsByVehicleType(vehicleType: VehicleType): Observable<Model[]> {
    let httpParams: HttpParams;

    httpParams = new HttpParams;

    httpParams = httpParams.append('vehicleType', VehicleType[vehicleType]);

    return this.httpClient.get<Model[]>(
      'http://' + this.apiConfig.server + '/models',
      {
        params: httpParams,
      }
    );
  }

  getOneModel(id: number): Observable<Model> {
    return this.httpClient.get<Model>('http://' + this.apiConfig.server + '/models/' + id);
  }

  create(creation: VehicleCreation): Observable<Vehicle> {
    return this.httpClient.post<Vehicle>(`http://${this.apiConfig.server}/vehicles`, creation);
  }

  createModel(creation: ModelCreation): Observable<Model> {
    return this.httpClient.post<Model>(`http://${this.apiConfig.server}/models`, creation);
  }

  createMake(creation: MakeCreation): Observable<Make> {
    return this.httpClient.post<Make>(`http://${this.apiConfig.server}/makes`, creation);
  }

  update(id: number, update: VehicleUpdate): Observable<void> {
    return this.httpClient.patch<void>(`http://${this.apiConfig.server}/vehicles/${id}`, update);
  }

  updateModel(id: number, newName: string): Observable<void> {
    return this.httpClient.patch<void>(`http://${this.apiConfig.server}/models/${id}`, newName);
  }

  updateMake(id: number, newName: string): Observable<void> {
    return this.httpClient.patch<void>(`http://${this.apiConfig.server}/makes/${id}`, newName);
  }

  deleteOne(id: number): Observable<void> {
    return this.httpClient.delete<void>(`http://${this.apiConfig.server}/vehicles/${id}`);
  }

  delete(ids: number[]): Observable<void> {
    let httpParams: HttpParams;

    httpParams = new HttpParams;

    ids.forEach(
      id =>
        httpParams = httpParams.append('ids', id.toString())
    );

    return this.httpClient.delete<void>(
      `http://${this.apiConfig.server}/vehicles`,
      {
        params: httpParams
      }
    );
  }

  deleteOneModel(modelId: number): Observable<void> {
    return this.httpClient.delete<void>(`http://${this.apiConfig.server}/models/${modelId}`);
  }

  deleteModels(modelIds: number[]): Observable<void> {
    let httpParams: HttpParams;

    httpParams = new HttpParams;

    modelIds.forEach(
      id =>
        httpParams = httpParams.append('ids', id.toString())
    );

    return this.httpClient.delete<void>(
      `http://${this.apiConfig.server}/models`,
      {
        params: httpParams
      }
    );
  }

  deleteOneMake(makeId: number): Observable<void> {
    return this.httpClient.delete<void>(`http://${this.apiConfig.server}/makes/${makeId}`);
  }

  deleteMakes(makeIds: number[]): Observable<void> {
    let httpParams: HttpParams;

    httpParams = new HttpParams;

    makeIds.forEach(
      id =>
        httpParams = httpParams.append(
        'ids', id.toString()
      )
    );

    return this.httpClient.delete<void>(
      `http://${this.apiConfig.server}/makes`,
      {
        params: httpParams
      }
    );
  }
}
