
export enum Type {
  OVERDUE_DURATION = "OverdueDurationUpdate",
}

export interface RentalUpdate {
  __type__: Type
}
