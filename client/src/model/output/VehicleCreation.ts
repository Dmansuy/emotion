export interface VehicleCreation {
  modelId: number
  serialNumber: string
  plateNumber: string
  numberOfKilometers: number
  purchaseDate: Date
  purchasePrice: number
  rentalPrice: number
  loyaltyPoints: number
}
