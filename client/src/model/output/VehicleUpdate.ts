
export enum Type {
  RENTALPRICE_AND_LOYALTYPOINTS = "RentalPriceAndLoyaltyPointsUpdate",
  KILOMETERAGE = "KilometerageUpdate",
  HIDDEN = "HiddenUpdate",
}

export interface VehicleUpdate {
  __type__: Type
}
