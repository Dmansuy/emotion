import {IdentifiableById} from "./IdentifiableById";
import {Model} from "./Model";

export interface Vehicle extends IdentifiableById {
  serialNumber: string
  plateNumber: string
  color: string
  numberOfKilometers: number
  purchaseDate: Date
  purchasePrice: number
  rentalPrice: number
  loyaltyPoints: number
  model: Model
  rentalIds: number[]
}
