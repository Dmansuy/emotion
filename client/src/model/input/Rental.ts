import {IdentifiableById} from "./IdentifiableById";
import {Vehicle} from "./Vehicle";
import {Customer} from "./Customer";

export interface Rental extends IdentifiableById {
  pickupDate: Date
  duration: number
  overdueDuration: number
  usingLoyaltyPoints: boolean
  vehicle: Vehicle
  customer: Customer
}
