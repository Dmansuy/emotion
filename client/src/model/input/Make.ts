import {IdentifiableById} from "./IdentifiableById";

export interface Make extends IdentifiableById {
  code: string
  name: string
  modelIds: number[]
}
