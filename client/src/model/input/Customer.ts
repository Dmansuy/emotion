import {User} from "./User";

export interface Customer extends User {
  firstName: string
  lastName: string
  birthDate: Date
  address: string
  city: string
  zipCode: string
  mailValidated: boolean
  loyaltyPoints: number
  rentalIds: number[]
}
