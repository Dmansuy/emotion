package com.albema.spring.http.interceptor;

import com.albema.spring.http.View;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

import static com.albema.common.util.ObjectUtils.ifNotNull;

@Component
class ViewAnnotationResolverInterceptor extends HandlerInterceptorAdapter {

    private final HashMap<HandlerMethod, String> handlerMethodViewNames;

    {
        handlerMethodViewNames = new HashMap<>();
    }

    public ViewAnnotationResolverInterceptor(
            RequestMappingHandlerMapping requestMappingHandlerMapping
    ) {
        requestMappingHandlerMapping.getHandlerMethods()
                .values()
                .forEach(
                        handlerMethod -> {
                            if (!handlerMethod.hasMethodAnnotation(ResponseBody.class)
                                    && handlerMethod.getBean()
                                    .getClass()
                                    .isAnnotationPresent(Controller.class))
                                ifNotNull(
                                        handlerMethod.getMethodAnnotation(View.class),
                                        view -> {
                                            handlerMethodViewNames.put(handlerMethod, view.value());
                                        }
                                );
                        }
                );

    }

    @Override
    @SuppressWarnings("all")
    public void postHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler,
            ModelAndView modelAndView
    ) throws Exception {
        if (handlerMethodViewNames.containsKey(handler))
            modelAndView.setViewName(handlerMethodViewNames.get(handler));
    }
}