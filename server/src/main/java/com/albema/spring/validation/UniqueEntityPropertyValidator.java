package com.albema.spring.validation;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;

@Component
class UniqueEntityPropertyValidator extends BusinessDataValidator<UniqueEntityProperty> {

    @Override
    @Transactional(readOnly = true)
    public boolean isValid(Serializable o, ConstraintValidatorContext constraintValidatorContext) {
        String stm;

        stm = "SELECT CASE COUNT(*) WHEN 0 THEN TRUE ELSE FALSE END "
            + "FROM " + getConstraintAnnotation().entityClass().getName() + " "
            + "WHERE " + getConstraintAnnotation().entityProperty() + "=:value";

        return (boolean) getCurrentSession().createQuery(stm)
                .setParameter("value", o)
                .uniqueResult();
    }

    UniqueEntityPropertyValidator(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}