package com.albema.spring.validation;

import com.albema.common.orm.entity.BaseEntity;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Repeatable(UniqueEntityProperties.List.class)
@Constraint(validatedBy = UniqueEntityPropertiesValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueEntityProperties {

    Class<? extends BaseEntity> entityClass();

    Mapping[] mappings();

    String message() default "UniqueEntityProperties";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @interface List {

        UniqueEntityProperties[] value();
    }
}
