package com.albema.spring.validation;

import com.albema.common.orm.entity.BaseEntity;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = UniqueEntityPropertyValidator.class)
@Target({
        ElementType.FIELD,
        ElementType.PARAMETER,
        ElementType.METHOD,
        ElementType.TYPE
})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueEntityProperty {

    Class<? extends BaseEntity> entityClass();

    String entityProperty();

    String message() default "UniqueEntityProperty";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default {};
}
