package com.albema.spring.validation;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;
import java.lang.reflect.Field;

import static java.lang.String.format;
import static java.util.Arrays.stream;
import static org.apache.commons.lang3.StringUtils.join;

@Component
class UniqueEntityPropertiesValidator extends BusinessDataValidator<UniqueEntityProperties> {

    @Override
    public boolean isValid(Serializable serializable, ConstraintValidatorContext constraintValidatorContext) {
        final Mapping[]      mappings;
        final String         stm;
        final Query<Boolean> query;
        final Class          serializableClass;

        Field field;

        if((mappings = getConstraintAnnotation().mappings()).length == 0)
            return true;

        stm = "SELECT CASE COUNT(*) WHEN 0 THEN TRUE ELSE FALSE END "
            + "FROM " + getConstraintAnnotation().entityClass().getName() + " "
            + "WHERE " + join(
                    stream(mappings).map(
                            mapping -> format(
                                    "%s=:%s",
                                    !mapping.entityProperty().isEmpty() ?
                                            mapping.entityProperty() :
                                            mapping.attribute(),
                                    mapping.attribute()
                            )
                    ).toArray(),
                " AND "
        );

        query = getCurrentSession().createQuery(stm, Boolean.class);

        serializableClass = serializable.getClass();

        for(final Mapping mapping : mappings)
            try {
                field = serializableClass.getDeclaredField(mapping.attribute());

                field.setAccessible(true);

                query.setParameter(
                        mapping.attribute(),
                        field.get(serializable)
                );

            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }

        return query.uniqueResult();
    }

    UniqueEntityPropertiesValidator(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
