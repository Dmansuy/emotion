package com.albema.common.orm.repository;

import com.albema.common.orm.entity.RecordableEntity;

public interface Saver<T extends RecordableEntity> {

    void save(T entity);

    @SuppressWarnings("unchecked")
    void save(T... entities);
}