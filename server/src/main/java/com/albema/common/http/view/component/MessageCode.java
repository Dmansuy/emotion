package com.albema.common.http.view.component;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({
        "WeakerAccess",
        "unused",
})
@Getter
@Setter
public final class MessageCode {

    private Type type;
    private String value;

    public MessageCode() {
        this.type = Type.INFO;
        this.value = "";
    }

    public MessageCode(Type type, String value) {
        this.type = type;
        this.value = value;
    }

    public MessageCode(String value) {
        this(Type.INFO, value);
    }

    @Override
    public String toString() {
        return value;
    }

    public enum Type {
        SUCCESS,
        INFO,
        WARNING,
        DANGER
    }
}
