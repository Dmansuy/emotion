package com.albema.common.http.view;

import lombok.Getter;

import org.springframework.http.HttpStatus;

@SuppressWarnings({
        "unused",
        "WeakerAccess"
})
public final class RedirectView extends org.springframework.web.servlet.view.RedirectView {

    @Getter
    private HttpStatus httpStatus;

    public RedirectView() {
        super("", true);
    }

    public RedirectView(String url) {
        this(url, HttpStatus.FOUND);
    }

    public RedirectView(String url, HttpStatus httpStatus) {
        super(url);
        setStatusCode(httpStatus);
        this.httpStatus = httpStatus;
    }
}