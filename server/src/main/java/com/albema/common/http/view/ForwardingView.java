package com.albema.common.http.view;

import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.view.AbstractUrlBasedView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static org.springframework.http.HttpStatus.OK;

@SuppressWarnings("WeakerAccess")
public final class ForwardingView extends AbstractUrlBasedView {

    private HttpStatus httpStatus;

    public ForwardingView() {
        this("", OK);
    }

    public ForwardingView(String uri) {
        super(uri);
    }

    public ForwardingView(String uri, HttpStatus httpStatus) {
        this(uri);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    @Override
    protected void renderMergedOutputModel(
            Map<String, Object> map,
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse
    ) throws Exception {
        final String internalUrl;

        internalUrl = httpServletRequest.getServletPath() + getUrl();

        httpServletRequest.setAttribute("action", internalUrl);

        map.forEach(httpServletRequest::setAttribute);

        httpServletResponse.setStatus(httpStatus.value());
        httpServletRequest.getRequestDispatcher(internalUrl)
                .forward(
                        httpServletRequest,
                        httpServletResponse
                );
    }
}