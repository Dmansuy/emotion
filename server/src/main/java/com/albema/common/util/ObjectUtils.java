package com.albema.common.util;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.albema.common.util.ArrayUtils.unshift;

public final class ObjectUtils {

    private ObjectUtils() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Utility class.");
    }

    static
    public Map<String, Serializable> toMap(Object object) {
        final Map<String, Serializable> map;

        Object value;

        map = new HashMap<>();

        for (final Field field : object.getClass().getDeclaredFields()) {

            field.setAccessible(true);
            try {
                value = field.get(object);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            map.put(field.getName(), (Serializable) value);
        }

        return map;
    }

    static
    public <T> T ifNull(
            T value,
            T other
    ) {
        return value != null ? value : other;
    }

    static
    public <T> T ifNull(
            T value,
            Supplier<T> supplier
    ) {
        return value != null ? value : supplier.get();
    }

    static
    public <T> T ifNull(
            T value,
            Function<Object, T> function,
            Object functionParameter
    ) {
        return value != null ? value : function.apply(functionParameter);
    }

    static
    public <O, T> T ifNotNull(O instance, Function<? super O, ? extends T> method, T nullSubstition) {
        return instance == null ? nullSubstition : method.apply(instance);
    }

    static
    public <O, T> T ifNotNull(O instance, Function<? super O, ? extends T> method) {
        return instance == null ? null : method.apply(instance);
    }

    static
    public <T> T ifNotNull(
            Object instance,
            Object[] params,
            Function<Object[], ? extends T> method,
            T nullSubstition
    ) {
        params = unshift(params, instance);
        return instance == null ? nullSubstition : method.apply(params);
    }

    static
    public <O> void ifNotNull(O instance, Consumer<? super O> method) {
        if (instance != null)
            method.accept(instance);
    }

    static
    public void ifNotNull(Object instance, Consumer<Object> consumer, Object value) {
        if (instance != null)
            consumer.accept(new Object[]{instance, value});
    }

    static
    public <T, U> void ifNotNull(T instance, BiConsumer<T, U> consumer, U value) {
        if (instance != null)
            consumer.accept(instance, value);
    }

    static
    public void ifNotNull(
            Object instance,
            Consumer<Object[]> method,
            Object[] params
    ) {
        if (instance != null) {
            params = unshift(params, instance);
            method.accept(params);
        }
    }

    static
    public void doIf(Runnable runnable, boolean bool) {
        if (bool)
            runnable.run();
    }
}