package com.albema.common.util;

import com.albema.common.dto.ToEntitiesConvertible;
import com.albema.common.orm.entity.identifiable.IdentifiableById;

import lombok.Getter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.Map;

@Getter
@ToString
@SuppressWarnings("unused")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "import-sources")
public final class ImportSources {

    @XmlElementWrapper(name = "domain-objects")
    @XmlElement(name = "domain-object")
    private DomainObject[] domainObjects;

    @Getter
    @ToString
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class DomainObject {

        @XmlAttribute(name = "class")
        private Class<? extends IdentifiableById> subjectClass;
        @XmlElementWrapper
        @XmlElement(name = "source")
        private Source[] sources;

        @Getter
        @ToString
        @XmlAccessorType(XmlAccessType.FIELD)
        public static class Source {

            @XmlAttribute
            private String name;
            private URI uri;
            private Key key;
            @XmlAttribute(name = "class")
            private Class<? extends ToEntitiesConvertible> outputClass;

            @Getter
            @ToString
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class URI {

                @XmlAttribute
                private String path;

                @XmlAttribute
                private String host;

                @XmlAttribute
                private boolean secured;

                @XmlElementWrapper
                @XmlElement(name = "param")
                private Map<String, ?> params;

                public String getPath() {
                    return path;
                }

                public String getHost() {
                    return host;
                }

                public boolean isSecured() {
                    return secured;
                }

                public Map<String, ?> getParams() {
                    return params;
                }
            }

            @Getter
            @ToString
            @XmlAccessorType(XmlAccessType.FIELD)
            public static class Key {

                @XmlAttribute
                private String name;
                @XmlAttribute
                private String value;
                @XmlAttribute
                private Mode mode;

                @XmlEnum
                public enum Mode {
                    @XmlEnumValue("in_uri")
                    IN_URI,
                    @XmlEnumValue("in_header")
                    IN_HEADER
                }
            }
        }
    }
}