package com.albema.common.util;

import com.albema.spring.validation.UniqueEntityProperty;


import java.lang.annotation.Annotation;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

import static java.lang.String.format;
import static java.lang.annotation.ElementType.FIELD;

import static org.apache.commons.lang3.ArrayUtils.contains;

public final class AnnotationUtils {

    static
    private final String FIELD_NOT_SUPPORTED_FORMAT;

    static {
        FIELD_NOT_SUPPORTED_FORMAT = "Annotation %s does not target field !";
    }

    private AnnotationUtils() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Utility class.");
    }

    static
    public Field getField(Class clazz, Annotation annotation) {

        if (
                annotation.getClass()
                        .isAnnotationPresent(Target.class)
                        && !contains(
                        annotation.getClass()
                                .getAnnotation(Target.class)
                                .value(),
                        FIELD
                )
                )
            throw new IllegalArgumentException(
                    format(
                            FIELD_NOT_SUPPORTED_FORMAT,
                            annotation.getClass()
                                    .getName()
                    )
            );

        for (final Field field : clazz.getDeclaredFields())
            if (
                    field.isAnnotationPresent(UniqueEntityProperty.class)
                            && annotation == field.getAnnotation(annotation.getClass())
                    )
                return field;


        return null;
    }
}