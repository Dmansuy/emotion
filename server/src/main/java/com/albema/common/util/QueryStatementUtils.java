package com.albema.common.util;

@SuppressWarnings("unused")
public final class QueryStatementUtils {

    private QueryStatementUtils() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Utility class.");
    }
}
