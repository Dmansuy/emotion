package com.albema.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static java.security.MessageDigest.getInstance;

public final class Hasher {

    private Hasher() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Utility class.");
    }

    static
    public String hash(String str, Algorithm algorith) throws NoSuchAlgorithmException {
        final MessageDigest d;
        final byte[] bytes;
        final StringBuilder result;

        d = getInstance(algorith.toString());

        d.reset();
        d.update(str.getBytes());

        bytes = d.digest();

        result = new StringBuilder();

        for (final byte b : bytes)
            result.append(
                    Integer.toString((b & 0xFF) + 0x100, 16)
                            .substring(1)
            );

        return result.toString();
    }

    public enum Algorithm {
        MD5,
        SHA_1,
        SHA_256,
        SHA_512;

        @Override
        public String toString() {
            return super.toString()
                    .replace('_', '-');
        }
    }
}