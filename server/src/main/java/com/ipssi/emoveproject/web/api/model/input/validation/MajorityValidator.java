package com.ipssi.emoveproject.web.api.model.input.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

import static java.time.LocalDate.*;

class MajorityValidator implements ConstraintValidator<Majority, LocalDate> {

    @Override
    public boolean isValid(
            LocalDate                  localDate,
            ConstraintValidatorContext constraintValidatorContext
    ) {
        return now().getYear() - localDate.getYear() >= 18;
    }

    @Override
    public void initialize(Majority constraintAnnotation) { }
}
