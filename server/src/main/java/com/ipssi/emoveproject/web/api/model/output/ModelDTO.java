package com.ipssi.emoveproject.web.api.model.output;

import com.google.gson.annotations.SerializedName;
import com.ipssi.emoveproject.business.mapping.Model;
import com.ipssi.emoveproject.business.mapping.VehicleType;

import java.math.BigInteger;

@SuppressWarnings("FieldCanBeLocal")
public class ModelDTO extends DTO<Model> {

    private final String code;

    private final VehicleType vehicleType;

    private final String name;

    @SerializedName("make")
    private final MakeDTO makeDTO;

    private final BigInteger[] vehicleIds;

    public ModelDTO(Model model) {
        super(model);

        code = model.getCode();
        vehicleType = model.getVehicleType();
        name = model.getName();

        makeDTO = new MakeDTO(model.getMake());

        vehicleIds = getIds(model.getVehicles());
    }
}
