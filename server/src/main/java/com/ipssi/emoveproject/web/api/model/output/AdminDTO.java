package com.ipssi.emoveproject.web.api.model.output;

import com.ipssi.emoveproject.business.mapping.Admin;

import static com.ipssi.emoveproject.web.api.model.output.UserDTO.Type.*;

@SuppressWarnings("WeakerAccess")
public class AdminDTO extends UserDTO<Admin> {

    public AdminDTO(Admin admin) {
        super(admin, ADMIN);
    }
}
