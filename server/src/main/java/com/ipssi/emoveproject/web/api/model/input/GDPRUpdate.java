package com.ipssi.emoveproject.web.api.model.input;

import lombok.*;

@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class GDPRUpdate extends CustomerUpdate {}
