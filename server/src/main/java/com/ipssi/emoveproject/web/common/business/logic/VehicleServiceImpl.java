package com.ipssi.emoveproject.web.common.business.logic;

import com.ipssi.emoveproject.business.logic.VehicleService;
import com.ipssi.emoveproject.business.mapping.Make;
import com.ipssi.emoveproject.business.mapping.Model;
import com.ipssi.emoveproject.business.mapping.Vehicle;
import com.ipssi.emoveproject.business.mapping.VehicleType;
import com.ipssi.emoveproject.business.repository.MakeDAO;
import com.ipssi.emoveproject.business.repository.ModelDAO;
import com.ipssi.emoveproject.business.repository.VehicleDAO;
import com.ipssi.emoveproject.business.repository.VehicleDAO.Search;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;

import static lombok.AccessLevel.PACKAGE;

@Service
@AllArgsConstructor(access = PACKAGE)
class VehicleServiceImpl implements VehicleService {

    private final VehicleDAO vehicleDAO;

    private final ModelDAO modelDAO;

    private final MakeDAO makeDAO;

    @Override
    @Transactional(readOnly = true)
    public List<Vehicle> get(BigInteger... ids) {
        return vehicleDAO.find(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Vehicle> getAll() {
        return vehicleDAO.find();
    }

    @Override
    @Transactional(readOnly = true)
    public Vehicle getOne(BigInteger id) {
        return vehicleDAO.find(id);
    }

    @Override
    @Transactional
    public Vehicle create(Creation creation) {
        final Model model;
        final Vehicle vehicle;

        model = modelDAO.find(creation.getModelId());

        vehicle = new Vehicle(
                creation.getSerialNumber(),
                model
        );

        vehicle.setColor(creation.getColor());
        vehicle.setNumberOfKilometers(creation.getNumberOfKilometers());
        vehicle.setPlateNumber(creation.getPlateNumber());
        vehicle.setPurchaseDate(creation.getPurchaseDate());
        vehicle.setPurchasePrice(creation.getPurchasePrice());
        vehicle.setRentalPrice(creation.getRentalPrice());
        vehicle.setLoyaltyPoints(creation.getLoyaltyPoints());

        vehicleDAO.save(vehicle);

        return vehicle;
    }

    @Override
    @Transactional
    public void delete(BigInteger... ids) {
        vehicleDAO.remove(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Model> getModels(String name) {
        return modelDAO.findByNameLike(name);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Model> getModels(BigInteger makeId) {
        return modelDAO.findByMakeId(makeId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Model> getModels(BigInteger makeId, String name) {
        return modelDAO.findByMakeIdAndNameLike(makeId, name);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Model> getModels(BigInteger... ids) {
        return modelDAO.find(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Model> getAllModels() {
        return modelDAO.find();
    }

    @Override
    @Transactional(readOnly = true)
    public Model getOneModel(BigInteger id) {
        return modelDAO.find(id);
    }

    @Override
    @Transactional
    public Model createModel(ModelCreation creation) {
        final Make make;
        final Model model;

        make = makeDAO.find(creation.getMakeId());

        model = new Model(creation.getCode(), creation.getVehicleType(), make);

        model.setName(creation.getName());

        modelDAO.save(model);

        return model;
    }

    @Override
    @Transactional
    public void deleteModels(BigInteger... ids) {
        modelDAO.remove(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Make> getMakes(BigInteger... ids) {
        return makeDAO.find(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Make> getMakes(String name) {
        return makeDAO.findByNameLike(name);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Make> getAllMakes() {
        return makeDAO.find();
    }

    @Override
    @Transactional(readOnly = true)
    public Make getOneMake(BigInteger id) {
        return makeDAO.find(id);
    }

    @Override
    @Transactional
    public Make createMake(MakeCreation creation) {
        final Make make;

        make = new Make(creation.getCode());

        make.setName(creation.getName());

        makeDAO.save(make);

        return make;
    }

    @Override
    @Transactional
    public void deleteMakes(BigInteger... ids) {
        makeDAO.remove(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Vehicle> get(Search search) {
        return vehicleDAO.find(search);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Model> getModels(VehicleType vehicleType) {
        return modelDAO.findByVehicleType(vehicleType);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Make> getMakes(VehicleType vehicleType) {
        return makeDAO.findByVehicleType(vehicleType);
    }

    @Override
    @Transactional
    public void update(BigInteger id, Update update) {
        final Vehicle vehicle;

        vehicle = vehicleDAO.find(id);

        if(vehicle == null)
            return;

        if(update instanceof HiddenUpdate)
            setByHidden(vehicle, (HiddenUpdate) update);
        else if(update instanceof KilometerageUpdate)
            setByKilometerage(vehicle, (KilometerageUpdate) update);
        else if(update instanceof RentalPriceAndLoyaltyPointsUpdate)
            setByLoyaltyPointsAndRentalPrice(
                    vehicle,
                    (RentalPriceAndLoyaltyPointsUpdate) update
            );

        vehicleDAO.save(vehicle);
    }

    private void setByHidden(Vehicle vehicle, HiddenUpdate update) {

    }

    private void setByKilometerage(Vehicle vehicle, KilometerageUpdate update) {
        vehicle.setNumberOfKilometers(update.getKilometerage());
    }

    private void setByLoyaltyPointsAndRentalPrice(
            Vehicle                           vehicle,
            RentalPriceAndLoyaltyPointsUpdate update
    ) {
        vehicle.setLoyaltyPoints(update.getLoyaltyPoints());
        vehicle.setRentalPrice(update.getRentalPrice());
    }

    @Override
    @Transactional
    public void updateMake(BigInteger id, String newName) {
        final Make make;

        make = makeDAO.find(id);

        if(make == null)
            return;

        make.setName(newName);

        makeDAO.save(make);
    }

    @Override
    @Transactional
    public void updateModel(BigInteger id, String newName) {
        final Model model;

        model = modelDAO.find(id);

        if(model == null)
            return;

        model.setName(newName);

        modelDAO.save(model);
    }
}
