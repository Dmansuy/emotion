package com.ipssi.emoveproject.web.api.model.input;

import com.ipssi.emoveproject.business.logic.VehicleService;
import lombok.*;

import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class KilometerageUpdate extends VehicleUpdate implements VehicleService.KilometerageUpdate {

    @PositiveOrZero
    private BigDecimal kilometerage;
}
