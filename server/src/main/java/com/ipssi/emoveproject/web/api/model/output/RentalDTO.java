package com.ipssi.emoveproject.web.api.model.output;

import com.fatboyindustrial.gsonjavatime.LocalDateConverter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.ipssi.emoveproject.business.mapping.Rental;

import java.time.LocalDate;

@SuppressWarnings("FieldCanBeLocal")
public class RentalDTO extends DTO<Rental> {

    @JsonAdapter(LocalDateConverter.class)
    private final LocalDate pickupDate;

    private final byte duration;

    private final Byte overdueDuration;

    private final boolean usingLoyaltyPoints;

    @SerializedName("vehicle")
    private final VehicleDTO vehicleDTO;

    @SerializedName("customer")
    private final CustomerDTO customerDTO;

    public RentalDTO(Rental rental) {
        super(rental);

        pickupDate = rental.getPickupDate();
        duration = rental.getNumberOfDays();
        overdueDuration = rental.getNumberOfDaysLate();
        usingLoyaltyPoints = rental.isUsingLoyaltyPoints();

        vehicleDTO = new VehicleDTO(rental.getVehicle());
        customerDTO = new CustomerDTO(rental.getCustomer());
    }
}
