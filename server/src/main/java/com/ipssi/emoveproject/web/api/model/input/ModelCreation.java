package com.ipssi.emoveproject.web.api.model.input;

import com.albema.spring.validation.ExistingEntity;
import com.albema.spring.validation.Mapping;
import com.albema.spring.validation.UniqueEntityProperties;
import com.ipssi.emoveproject.business.logic.VehicleService;
import com.ipssi.emoveproject.business.mapping.Make;
import com.ipssi.emoveproject.business.mapping.Model;
import com.ipssi.emoveproject.business.mapping.VehicleType;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;

@Getter
@ToString
@EqualsAndHashCode
@UniqueEntityProperties(
        entityClass = Model.class,
        mappings = {
                @Mapping(attribute = "makeId", entityProperty = "make.id"),
                @Mapping(attribute = "code"),
        }
)
@NoArgsConstructor
@AllArgsConstructor
public class ModelCreation implements VehicleService.ModelCreation, Serializable {

    @ExistingEntity(Make.class)
    private BigInteger makeId;

    @NotNull
    @Length(min = 1, max = 4)
    private String code;

    @NotNull
    private VehicleType vehicleType;

    private String name;
}
