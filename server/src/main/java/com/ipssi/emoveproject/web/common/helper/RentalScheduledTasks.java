package com.ipssi.emoveproject.web.common.helper;

import com.ipssi.emoveproject.business.logic.RentalService;
import com.ipssi.emoveproject.business.mapping.Rental;
import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

import static java.time.LocalDate.now;
import static java.util.Locale.*;
import static lombok.AccessLevel.PACKAGE;

@Component
@AllArgsConstructor(
        access = PACKAGE
)
class RentalScheduledTasks {

    private final RentalService rentalService;

    private final HtmlMailer    htmlMailer;

    private final MessageSource messageSource;

    @Scheduled(
            cron = "${cronExpressions.proceedOverdueDurations}"
    )
    void proceedOverdueDurations() throws Exception {
        final LocalDate                                 currentDate;
        final List<Rental>                              rentals;
        final HashMap<String, Function<Rental, Object>> lazyModel;

        currentDate = now();

        rentals = rentalService.get(currentDate, null);

        lazyModel = new HashMap<>();

        lazyModel.put(
                "customerFirstName",
                rental -> rental.getCustomer()
                                .getFirstName()
        );
        lazyModel.put(
                "customerLastName", rental -> rental.getCustomer()
                                                    .getLastName()
        );
        lazyModel.put(
                "makeName",
                rental -> rental.getVehicle()
                                .getModel()
                                .getMake()
                                .getName()
        );
        lazyModel.put(
                "modelName",
                rental -> rental.getVehicle()
                                .getModel()
                                .getName()
        );
        lazyModel.put("rentalPickupDate", Rental::getPickupDate);
        lazyModel.put(
                "rentalOverdueDuration",
                rental -> currentDate.toEpochDay()
                        - rental.getPickupDate()
                                .toEpochDay()
                        + rental.getNumberOfDays()
        );
        lazyModel.put("locale", rental -> getDefault());

        htmlMailer.send(
                "overdue-duration",
                messageSource.getMessage(
                    "overdueDuration.title",
                    new Object[0],
                    getDefault()
                ),
                rental -> rental.getCustomer()
                                .getEmailAddress(),
                lazyModel,
                rentals
        );
    }
}
