package com.ipssi.emoveproject.web.api.model.input;

import com.ipssi.emoveproject.business.mapping.VehicleType;
import com.ipssi.emoveproject.business.repository.VehicleDAO.Search;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class VehicleSearch implements Search, Serializable {

    private VehicleType modelVehicleType;

    private BigInteger  modelId;

    private BigInteger  modelMakeId;

    @DateTimeFormat(iso = DATE)
    private LocalDate   rentalPickupDate;

    private Byte        rentalNumberOfDay;
}
