package com.ipssi.emoveproject.web.api.model.input.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.time.temporal.ChronoUnit;

import static java.time.temporal.ChronoUnit.DAYS;

@Constraint(validatedBy = RentalOverlapForbiddenValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RentalOverlapForbidden {

    String pickupDateAttribute() default "pickupDate";

    String vehicleIdAttribute() default "vehicleId";

    String durationAttribute() default "duration";

    ChronoUnit chronoUnit() default DAYS;

    String message() default "RentalOverlapForbidden";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
