package com.ipssi.emoveproject.web.validation.model.input.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = ValidationCodeValidator.class)
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidationCode {
    String message() default "ValidationCode";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
