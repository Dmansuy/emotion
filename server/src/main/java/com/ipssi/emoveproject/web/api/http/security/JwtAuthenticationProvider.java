package com.ipssi.emoveproject.web.api.http.security;

import com.albema.common.http.error.CustomHttpExceptions;
import com.ipssi.emoveproject.business.logic.UserService;
import com.ipssi.emoveproject.business.mapping.Admin;
import com.ipssi.emoveproject.business.mapping.Customer;
import com.ipssi.emoveproject.business.mapping.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import static lombok.AccessLevel.PACKAGE;
import static org.springframework.security.core.authority.AuthorityUtils.createAuthorityList;

@Component
@RequiredArgsConstructor(access = PACKAGE)
class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private final JwtManager jwtManager;

    private final UserService userService;

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(String s, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

        final String token;
        final User parsedUser;
        final Collection<GrantedAuthority> authorityList;

        if(usernamePasswordAuthenticationToken instanceof JwtAuthenticationToken) {
            token = ((JwtAuthenticationToken) usernamePasswordAuthenticationToken).getToken();

            if(!jwtManager.validateToken(token))
                throw new CustomHttpExceptions.BadRequestException();

            parsedUser = jwtManager.parseToken(token);

        } else {
            try {

                parsedUser = userService.getOne(
                        usernamePasswordAuthenticationToken.getPrincipal()
                                                           .toString(),
                        usernamePasswordAuthenticationToken.getCredentials()
                                                           .toString()
                );

            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
        if (parsedUser == null)
            throw new CustomHttpExceptions.BadRequestException();

        authorityList = createAuthorityList((
                        parsedUser instanceof Admin ?
                                Admin.class :
                                Customer.class
                ).getSimpleName()
                 .toUpperCase());

        return new org.springframework.security.core.userdetails.User(
                parsedUser.getId().toString(),
                parsedUser.getEmailAddress(),
                authorityList
        );
    }
}
