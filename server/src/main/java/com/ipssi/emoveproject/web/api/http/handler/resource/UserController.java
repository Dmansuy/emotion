package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.business.logic.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;

import static org.springframework.http.HttpStatus.NO_CONTENT;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
class UserController {

    private final UserService userService;

    @PreAuthorize("isAuthenticated()")
    @ResponseStatus(NO_CONTENT)
    @PatchMapping("/{id}")
    public void onPatch(
            @PathVariable                 BigInteger id,
            @RequestBody @NotBlank @Valid String     newPassword
    ) throws NoSuchAlgorithmException {
        userService.update(id, newPassword);
    }
}
