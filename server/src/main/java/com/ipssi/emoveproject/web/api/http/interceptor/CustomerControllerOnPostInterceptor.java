package com.ipssi.emoveproject.web.api.http.interceptor;

import com.google.gson.Gson;
import com.ipssi.emoveproject.business.logic.UserService;
import com.ipssi.emoveproject.web.api.model.input.CustomerCreation;
import com.ipssi.emoveproject.web.api.model.output.CustomerDTO;
import com.ipssi.emoveproject.web.common.helper.HtmlMailer;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Objects;

import static com.albema.common.util.Hasher.Algorithm.MD5;
import static com.albema.common.util.Hasher.hash;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Component
class CustomerControllerOnPostInterceptor implements HandlerInterceptor {

    private final static Gson gson;

    static {
        gson = new Gson();
    }

    private final String        validationServer;

    private final UserService   userService;

    private final HtmlMailer    htmlMailer;

    private final MessageSource messageSource;

    @Override
    public void afterCompletion(
            HttpServletRequest  request,
            HttpServletResponse response,
            Object              handler,
            Exception           ex
    ) throws Exception {
        final CustomerCreation        customerCreation;
        final HashMap<String, Object> model;

        if(!(handler instanceof HandlerMethod)
        || !((HandlerMethod) handler).getMethod()
                                     .getReturnType()
                                     .equals(CustomerDTO.class))
            return;

        if(Objects.equals(POST,
                          RequestMethod.valueOf(request.getMethod()))
        && Objects.equals(
                CREATED,
                HttpStatus.valueOf(response.getStatus()))) {
            customerCreation = gson.fromJson(
                    IOUtils.toString(
                            request.getReader()
                    ),
                    CustomerCreation.class
            );

            model = new HashMap<>();

            model.put("locale", request.getLocale());
            model.put(
                    "customerId",
                    userService.getOne(customerCreation.getEmailAddress())
                               .getId()
                               .toString()
            );
            model.put("firstName", customerCreation.getFirstName());
            model.put("lastName", customerCreation.getLastName());
            model.put("emailAddress", customerCreation.getEmailAddress());
            model.put("validationServer", validationServer);
            model.put(
                    "validationCode",
                    hash(
                    customerCreation.getEmailAddress() +
                        customerCreation.getFirstName() +
                        customerCreation.getBirthDate(),
                        MD5
                    )
            );

            htmlMailer.send(
                    "email-validation",
                    messageSource.getMessage(
                            "emailValidation.title",
                            new Object[0],
                            request.getLocale()
                    ),
                    customerCreation.getEmailAddress(),
                    model
            );
        }
    }

    CustomerControllerOnPostInterceptor(
            @Value("#{@systemProperties['validation.server']}") String        validationServer,
                                                                UserService   userService,
                                                                HtmlMailer    htmlMailer,
                                                                MessageSource messageSource
    ) {
        this.validationServer = validationServer;
        this.userService      = userService;
        this.htmlMailer       = htmlMailer;
        this.messageSource    = messageSource;
    }
}
