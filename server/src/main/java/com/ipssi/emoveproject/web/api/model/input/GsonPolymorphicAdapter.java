package com.ipssi.emoveproject.web.api.model.input;

import com.albema.common.http.error.CustomHttpExceptions;
import com.google.gson.*;
import com.ipssi.emoveproject.web.api.model.output.DTO;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.List;

import static com.albema.common.util.ReflectionUtils.findClasses;
import static java.lang.reflect.Modifier.isAbstract;

class GsonPolymorphicAdapter implements JsonDeserializer<Serializable>, JsonSerializer<Serializable> {

    private static final String TYPE_PROPERTY_NAME = "__type__";

    private static final List<Class<?>> classes;

    static {
        try {
            classes = findClasses(
                    GsonPolymorphicAdapter.class.getPackage(),
                    Serializable.class,
                    new Class[] {
                            GsonPolymorphicAdapter.class
                    }
            );

            classes.removeIf(clazz -> isAbstract(clazz.getModifiers()));

        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Serializable deserialize(
            JsonElement                json,
            Type                       typeOfT,
            JsonDeserializationContext context
    ) throws JsonParseException {
        final String                        typeAsString;
        final Class<? extends Serializable> clazz;

        if(!json.getAsJsonObject().has(TYPE_PROPERTY_NAME))
            throw new CustomHttpExceptions.BadRequestException();

        typeAsString = json.getAsJsonObject()
                           .get(TYPE_PROPERTY_NAME)
                           .getAsString()
                           .replace("_", "");

        json.getAsJsonObject().remove(TYPE_PROPERTY_NAME);

        clazz = (Class<? extends Serializable>) classes.stream()
                                                       .filter(
                                                               c -> c.getSimpleName()
                                                                     .equalsIgnoreCase(typeAsString)
                                                       )
                                                       .findFirst()
                                                       .get();

        return context.deserialize(json, clazz);
    }

    @Override
    public JsonElement serialize(
            Serializable             serializable,
            Type                     type,
            JsonSerializationContext jsonSerializationContext
    ) {
        final Class<? extends Serializable> clazz;
        final JsonElement jsonElement;

        clazz = serializable.getClass();

        jsonElement = jsonSerializationContext.serialize(serializable, clazz);
        jsonElement.getAsJsonObject().addProperty(
                TYPE_PROPERTY_NAME,
                clazz.getSimpleName()
                     .replace(
                            DTO.class.getSimpleName(),
                            ""
                     )
                     .replaceAll("(.)(\\p{Upper})", "$1_$2")
                     .toUpperCase()
        );

        return jsonElement;
    }
}
