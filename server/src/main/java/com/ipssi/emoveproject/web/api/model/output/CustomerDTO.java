package com.ipssi.emoveproject.web.api.model.output;

import com.fatboyindustrial.gsonjavatime.LocalDateConverter;
import com.google.gson.annotations.JsonAdapter;
import com.ipssi.emoveproject.business.mapping.Customer;

import java.math.BigInteger;
import java.time.LocalDate;

import static com.ipssi.emoveproject.web.api.model.output.UserDTO.Type.*;

@SuppressWarnings("FieldCanBeLocal")
public class CustomerDTO extends UserDTO<Customer> {

    private final String firstName;

    private final String lastName;

    @JsonAdapter(LocalDateConverter.class)
    private final LocalDate birthDate;

    private final String address;

    private final String city;

    private final String zipCode;

    private final boolean mailValidated;

    private final Integer loyaltyPoints;

    private final BigInteger[] rentalIds;

    public CustomerDTO(Customer customer) {
        super(customer, CUSTOMER);

        firstName = customer.getFirstName();
        lastName = customer.getLastName();
        birthDate = customer.getBirthDate();
        address = customer.getAddress();
        city = customer.getCity();
        zipCode = customer.getZipCode();
        mailValidated = customer.isMailValidated();
        loyaltyPoints = customer.getLoyaltyPoints();

        rentalIds = getIds(customer.getRentals());
    }
}
