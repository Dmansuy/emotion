package com.ipssi.emoveproject.web.api.model.input.validation;

import com.albema.common.orm.entity.Void;
import com.albema.spring.validation.BusinessDataValidator;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;
import java.lang.reflect.Field;

class EnoughLoyaltyPointsValidator extends BusinessDataValidator<EnoughLoyaltyPoints> {

    @Override
    public boolean isValid(Serializable serializable, ConstraintValidatorContext constraintValidatorContext) {
        final Class<? extends Serializable> serializableClass;

        final Field durationField;
        final Field vehicleIdField;
        final Field customerIdField;

        final String stm;

        final Query<Boolean> query;

        serializableClass = serializable.getClass();

        try {
            durationField = serializableClass.getDeclaredField(getConstraintAnnotation().durationAttribute());
            vehicleIdField = serializableClass.getDeclaredField(getConstraintAnnotation().vehicleIdAttribute());
            customerIdField = serializableClass.getDeclaredField(getConstraintAnnotation().customerIdAttribute());

            durationField.setAccessible(true);
            vehicleIdField.setAccessible(true);
            customerIdField.setAccessible(true);


            stm = "SELECT areEnoughLoyaltyPoints(CAST(:duration AS short), CAST(:customerId AS long), CAST(:vehicleId AS long)) " +
                    "FROM " + Void.class.getName();

            query = getCurrentSession().createQuery(stm, Boolean.class);

            query.setParameter("duration", durationField.get(serializable));

            query.setParameter("vehicleId", vehicleIdField.get(serializable));

            query.setParameter("customerId", customerIdField.get(serializable));

        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        return !query.uniqueResult();
    }

    protected EnoughLoyaltyPointsValidator(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
