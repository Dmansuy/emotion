package com.ipssi.emoveproject.web.api.model.input;

import com.albema.spring.validation.ExistingEntity;
import com.albema.spring.validation.UniqueEntityProperty;
import com.ipssi.emoveproject.business.logic.VehicleService.Creation;
import com.ipssi.emoveproject.business.mapping.Model;
import com.ipssi.emoveproject.business.mapping.Vehicle;
import lombok.*;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import static java.lang.Integer.*;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class VehicleCreation implements Creation, Serializable {

    @NotNull
    @ExistingEntity(Model.class)
    private BigInteger modelId;

    @NotNull
    @Pattern(regexp = "^[A-Z0-9]{17}$")
    @UniqueEntityProperty(
            entityClass    = Vehicle.class,
            entityProperty = "serialNumber"
    )
    private String serialNumber;

    @Pattern(regexp = "^[A-Z]{2}\\d{3}[A-Z]{2}$")
    private String plateNumber;

    private String color;

    @PositiveOrZero
    private BigDecimal numberOfKilometers;

    @PastOrPresent
    private LocalDate purchaseDate;

    @Digits(integer = MAX_VALUE, fraction = 2)
    private Float purchasePrice;

    @Digits(integer = MAX_VALUE, fraction = 2)
    private Float rentalPrice;

    @PositiveOrZero
    private Integer loyaltyPoints;
}
