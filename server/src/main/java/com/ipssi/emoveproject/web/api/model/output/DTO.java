package com.ipssi.emoveproject.web.api.model.output;

import com.albema.common.orm.entity.identifiable.IdentifiableById;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import static java.lang.reflect.Array.newInstance;

public abstract class DTO<E extends IdentifiableById> implements Serializable {

    @SuppressWarnings("FieldCanBeLocal")
    private BigInteger id;

    protected DTO(E entity) {
        if (entity.getId() == null)
            throw new IllegalArgumentException("Entity must have been persisted !");

        id = entity.getId();
    }

    static
    public <E extends IdentifiableById, P extends DTO<E>>
    List<P> fromCollection(
            Collection<E> entities,
            Function<E, P> function
    ) {
        final List<P> result;

        result = new ArrayList<>();

        for (E entity : entities)
            result.add(function.apply(entity));

        return result;
    }

    static
    @SuppressWarnings("unchecked")
    protected <E extends Serializable, T> T[] aggregate(
            Collection<E> entities,
            Function<E, T> function,
            Class<T> aggregatedType
    ) {
        final T[] aggregated;
        int i;

        aggregated = (T[]) newInstance(aggregatedType, entities.size());
        i = 0;

        for (E entity : entities)
            aggregated[i++] = function.apply(entity);

        return aggregated;
    }

    @SuppressWarnings("WeakerAccess")
    static
    protected BigInteger[] getIds(Collection<? extends IdentifiableById> collection) {
        final BigInteger[] ids;
        int i;

        ids = new BigInteger[collection.size()];
        i = 0;

        for (IdentifiableById element : collection)
            ids[i++] = element.getId();

        return ids;
    }
}
