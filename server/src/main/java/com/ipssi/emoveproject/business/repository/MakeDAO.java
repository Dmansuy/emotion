package com.ipssi.emoveproject.business.repository;

import com.albema.common.orm.repository.Finder;
import com.albema.common.orm.repository.Remover;
import com.albema.common.orm.repository.Saver;
import com.ipssi.emoveproject.business.mapping.Make;
import com.ipssi.emoveproject.business.mapping.VehicleType;

import java.util.List;

public interface MakeDAO extends Finder<Make>, Saver<Make>, Remover<Make> {
    List<Make> findByNameLike(String nameLike);

    List<Make> findByVehicleType(VehicleType vehicleType);
}
