package com.ipssi.emoveproject.business.logic;

import com.ipssi.emoveproject.business.mapping.Rental;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

public interface RentalService {

    List<Rental> get(BigInteger... ids);

    List<Rental> getAll();

    Rental getOne(BigInteger id);

    List<Rental> getByCustomerId(BigInteger customerId);

    Rental getOneByCustomerId(BigInteger rentalId, BigInteger customerId);

    Rental create(Creation creation);

    void update(BigInteger id, Update update);

    void delete(BigInteger... ids);

    List<Rental> get(
            LocalDate maxExclusiveDropOffDate,
            Integer   overdueDuration
    );

    interface Creation {

        BigInteger getVehicleId();

        BigInteger getCustomerId();

        LocalDate getPickupDate();

        byte getDuration();

        boolean isUsingLoyaltyPoints();
    }

    interface Update {

    }

    interface OverdueDurationUpdate {

        Byte getOverdueDuration();
    }
}
