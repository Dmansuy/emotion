package com.ipssi.emoveproject.business.mapping;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

import static lombok.AccessLevel.PROTECTED;

@Entity
@Table(name = "admins")
@NoArgsConstructor(access = PROTECTED)
public class Admin extends User {

    public Admin(String emailAddress, String hash) {
        super(emailAddress);
        setHash(hash);
    }
}