package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.web.api.model.input.*;
import com.ipssi.emoveproject.web.common.ApiServletContextConsumer;
import com.ipssi.emoveproject.web.common.http.handler.ControllerTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.albema.common.util.ObjectUtils.toMap;
import static java.math.BigDecimal.valueOf;
import static java.math.BigInteger.ONE;
import static java.time.LocalDate.of;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class VehicleControllerTest extends ControllerTest implements ApiServletContextConsumer {

    @Test
    public void should_return_status_200_when_on_get() throws Exception {
/*
        getMockMvc().perform(get("/vehicles"))
                .andExpect(status().isOk());
*/
        getMockMvc().perform(
                get("/vehicles?search&modelVehicleType=CAR&rentalNumberOfDay=3&modelId=1")
        ).andExpect(status().isOk());
/*
        getMockMvc().perform(get("/vehicles/1"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/vehicles").param("ids", "1", "2"))
                .andExpect(status().isOk());*/
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_status_201_when_on_post() throws Exception {

        final VehicleCreation vehicleCreation;

        vehicleCreation = new VehicleCreation(
                ONE, "1GCVKPEC2EZ143580",
                "KE935MG", "Verte",
                valueOf(1000L), of(2017, 5, 18),
                12000f, 120f, 10
        );

        getMockMvc().perform(
                post("/vehicles").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(vehicleCreation)
                        )
        ).andExpect(
                status().isCreated()
        );
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_status_204_when_onPatch() throws Exception {

        final Map<String, String> vehicleUpdateMap;

        vehicleUpdateMap = new HashMap<>();

        toMap(
                new HiddenUpdate(true)
        ).forEach(
                (k, v) -> vehicleUpdateMap.put(k, v.toString())
        );

        vehicleUpdateMap.put("__type__", HiddenUpdate.class.getSimpleName());

        getMockMvc().perform(
                patch("/vehicles/2").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(vehicleUpdateMap)
                        )
        ).andExpect(
                status().isNoContent()
        );

        vehicleUpdateMap.clear();

        toMap(
                new KilometerageUpdate(BigDecimal.valueOf(593355, 4))
        ).forEach((k, v) -> vehicleUpdateMap.put(k, v.toString()));

        vehicleUpdateMap.put("__type__", KilometerageUpdate.class.getSimpleName());

        getMockMvc().perform(
                patch("/vehicles/2").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(vehicleUpdateMap)
                        )
        ).andExpect(
                status().isNoContent()
        );

        toMap(
                new RentalPriceAndLoyaltyPointsUpdate(50.99f, 500)
        ).forEach((k, v) -> vehicleUpdateMap.put(k, v.toString()));

        vehicleUpdateMap.put("__type__", RentalPriceAndLoyaltyPointsUpdate.class.getSimpleName());

        getMockMvc().perform(
                patch("/vehicles/2").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(vehicleUpdateMap)
                        )
        ).andExpect(
                status().isNoContent()
        );
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_status_204_when_on_delete() throws Exception {

        getMockMvc().perform(delete("/vehicles/1"))
                .andExpect(status().isNoContent());

        getMockMvc().perform(delete("/vehicles").param("ids", "1", "2"))
                .andExpect(status().isNoContent());
    }
}