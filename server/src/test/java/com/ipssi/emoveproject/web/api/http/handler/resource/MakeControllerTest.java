package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.web.common.ApiServletContextConsumer;
import com.ipssi.emoveproject.web.common.http.handler.ControllerTest;
import com.ipssi.emoveproject.web.api.model.input.MakeCreation;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MakeControllerTest extends ControllerTest implements ApiServletContextConsumer {

    @Test
    public void should_return_status_200_when_on_get() throws Exception {

        getMockMvc().perform(get("/makes"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/makes/1"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/makes").param("ids", "1", "2"))
                    .andExpect(status().isOk());

        getMockMvc().perform(get("/makes").param("name", "to"))
                    .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_status_201_when_on_post() throws Exception {

        final MakeCreation makeCreation;

        makeCreation = new MakeCreation("TOTO", "Totoyoyotata");

        getMockMvc().perform(
                post("/makes").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(makeCreation)
                        )
        ).andExpect(
                status().isCreated()
        );
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_status_204_when_on_delete() throws Exception {

        getMockMvc().perform(delete("/makes/1"))
                .andExpect(status().isNoContent());

        getMockMvc().perform(delete("/makes?ids=2&ids=3"))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_status_204_when_onPatch() throws Exception {

        final String newName;

        newName = "patched name";

        getMockMvc().perform(
                patch("/makes/1")
                        .content(
                                newName
                        )
        ).andExpect(
                status().isNoContent()
        );
    }
}