package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.web.api.model.input.AdminCreation;
import com.ipssi.emoveproject.web.common.ApiServletContextConsumer;
import com.ipssi.emoveproject.web.common.http.handler.ControllerTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static java.util.Locale.FRANCE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AdminControllerTest extends ControllerTest implements ApiServletContextConsumer {

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_200_when_onGet() throws Exception {
        getMockMvc().perform(get("/admins"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/admins/3"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/admins").param("ids", "3"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_201_when_onPost() throws Exception {
        final AdminCreation adminCreation;

        adminCreation = new AdminCreation(
                "john-doe-junior@foo.bar", "secret"
        );

        getMockMvc().perform(
                post("/admins").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(adminCreation)
                        ).locale(FRANCE)
        ).andExpect(
                status().isCreated()
        );
    }
}