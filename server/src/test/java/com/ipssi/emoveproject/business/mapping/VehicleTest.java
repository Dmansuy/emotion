package com.ipssi.emoveproject.business.mapping;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.ipssi.emoveproject.business.mapping.VehicleType.SCOOTER;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class VehicleTest {

    private final Make make = new Make("C569");

    private final VehicleType vehicleType = SCOOTER;

    private final Model model = new Model("C2569", vehicleType, make);

    private final String seriesNumber = "256D23D";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void should_create_vehicle() {
        // Given When
        Vehicle vehicle = new Vehicle(seriesNumber, model);

        // Then
        assertThat(vehicle.getModel()).isEqualTo(model);
        assertThat(vehicle.getSerialNumber()).isEqualTo(seriesNumber);
    }


    @Test()
    public void should_throw_exception_when_model_is_null() {
        // Expect
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Model cannot be null");

        // When
        new Vehicle(seriesNumber, null);
    }

    @Test
    public void should_throw_exception_when_series_number_is_blank() {
        // Expect
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Serial number cannot be null");

        // When
        new Vehicle("", model);
    }

    @Test
    public void should_throw_exception_when_series_number_is_null() {
        // Expect
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Serial number cannot be null");

        // When
        new Vehicle(null, model);
    }
}
